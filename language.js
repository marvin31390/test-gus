const LOCALE_KEY = "language"
let setLanguage = localStorage.getItem(LOCALE_KEY)
let languages = setLanguage ? setLanguage : "fr"

switch (languages) {
    case "fr" : language = dataFr;
    break;
    case "es" : language = dataEs;
    break;
    case "en" : language = dataEn;
    break;
    case "kr" : language = dataKr;
    break;
    default : language = dataFr;
    break;
}



function changeLanguage (language) {
    localStorage.setItem(LOCALE_KEY, language);
    window.location.reload();
    
}

// changer la langue du titre de la page
const titleSelect = document.getElementById("title_language")
const titlelanguageContent = document.createTextNode(language[0].Name)
titleSelect.appendChild(titlelanguageContent)

// changer la langue des catégories
const categories = document.getElementById("categories")
if(categories === null){}else{
for (let i = 0; i < dataCategories.length; i++)
{
    let titleCategories = document.createElement("div")
    let imageCategories = document.createElement("img")
    imageCategories.setAttribute("src", `http://localhost:1337${dataCategories[i].Icon.url}`)
    imageCategories.setAttribute("class", "image_categories")
    let titleCategoriesLink = document.createElement("a")
    let titleCategoriesContent
    titleCategories.appendChild(imageCategories)
    titleCategories.appendChild(titleCategoriesLink)
    titleCategories.setAttribute("class", "categorie")
    categories.appendChild(titleCategories)
    switch (dataCategories[i].Id) {  case 1 :
       titleCategoriesContent = document.createTextNode(language[3].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/numeros.html")
       break; case 2 : 
       titleCategoriesContent = document.createTextNode(language[4].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#Accueilli")
       break; case 3 :
       titleCategoriesContent = document.createTextNode(language[5].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html")
      break; case 4 : 
       titleCategoriesContent = document.createTextNode(language[6].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#Domicilié")
      break; case 5 :
       titleCategoriesContent = document.createTextNode(language[7].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#Soigner")
      break; case 6 :
       titleCategoriesContent = document.createTextNode(language[8].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#Laver")
      break; case 7 : 
       titleCategoriesContent = document.createTextNode(language[9].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#Nourrir")
      break; case 8 : 
       titleCategoriesContent = document.createTextNode(language[10].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#Habiller")
      break; case 9 :
       titleCategoriesContent = document.createTextNode(language[11].Name)
       titleCategoriesLink.setAttribute("href", "file:///home/massot/Bureau/stage%202/front/po/carte.html#hebergement")
      break; } 
    titleCategoriesLink.appendChild(titleCategoriesContent)

}
}