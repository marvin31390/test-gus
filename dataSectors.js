dataSectors = [
    {
        "Name": "Saint-Gaudens",
        "Logocomcom": {
            "id": 13,
            "name": "coeur.png",
            "alternativeText": "",
            "caption": "",
            "width": 225,
            "height": 225,
            "formats": {
                "thumbnail": {
                    "name": "thumbnail_coeur.png",
                    "hash": "thumbnail_coeur_f211020a59",
                    "ext": ".png",
                    "mime": "image/png",
                    "width": 156,
                    "height": 156,
                    "size": 12.96,
                    "path": null,
                    "url": "/uploads/thumbnail_coeur_f211020a59.png"
                }
            },
            "hash": "coeur_f211020a59",
            "ext": ".png",
            "mime": "image/png",
            "size": 4.85,
            "url": "/uploads/coeur_f211020a59.png",
            "previewUrl": null,
            "provider": "local",
            "provider_metadata": null,
            "created_at": "2021-06-01T07:24:14.000Z",
            "updated_at": "2021-06-01T07:24:14.000Z"
        },
        "Logostructure": {
            "id": 14,
            "name": "fb.png",
            "alternativeText": "",
            "caption": "",
            "width": 225,
            "height": 225,
            "formats": {
                "thumbnail": {
                    "name": "thumbnail_fb.png",
                    "hash": "thumbnail_fb_8f25c595b4",
                    "ext": ".png",
                    "mime": "image/png",
                    "width": 156,
                    "height": 156,
                    "size": 3.03,
                    "path": null,
                    "url": "/uploads/thumbnail_fb_8f25c595b4.png"
                }
            },
            "hash": "fb_8f25c595b4",
            "ext": ".png",
            "mime": "image/png",
            "size": 1.24,
            "url": "/uploads/fb_8f25c595b4.png",
            "previewUrl": null,
            "provider": "local",
            "provider_metadata": null,
            "created_at": "2021-06-01T07:24:31.000Z",
            "updated_at": "2021-06-01T07:24:31.000Z"
        }
    }
]